#!/bin/bash

set -eu

export GOPATH=$(
	cd ../../
	cygpath -w "$(pwd)"
)

sanitise() {
	local tmp=$(mktemp)
	cat "$1" | perl -pe 's~C:.Users.......Documents.DEV.~C:/xxxxxxxxxxxxxxxxxxxxxxxxx/~g' > "$tmp"
	mv "$tmp" "$1"
	chmod 755 "$1"
}

main() {

	local version=""
	read -p "Enter version string (blank for timestamp)> " version
	if [[ $version == "" ]] ; then
		version=$(date +%s)
	fi
	echo "Using '${version}' as the version."

	if [[ -f nmdc-log-service.exe ]] ; then
		rm ./nmdc-log-service.exe
	fi
	if [[ -f nmdc-log-service ]] ; then
		rm ./nmdc-log-service
	fi
	
	echo "Building win64..."
	GOARCH=amd64 GOOS=windows go build -a -ldflags -s -o nmdc-log-service.exe
	sanitise nmdc-log-service.exe
	7z a -mx9 nmdc-log-service-${version}-win64.7z nmdc-log-service.exe >/dev/null
	rm ./nmdc-log-service.exe
	
	echo "Building win32..."
	GOARCH=386 GOOS=windows go build -a -ldflags -s -o nmdc-log-service.exe
	sanitise nmdc-log-service.exe
	7z a -mx9 nmdc-log-service-${version}-win32.7z nmdc-log-service.exe >/dev/null
	rm ./nmdc-log-service.exe
	
	echo "Building linux64..."
	GOARCH=amd64 GOOS=linux go build -a -ldflags -s -o nmdc-log-service
	sanitise nmdc-log-service
	XZ_OPT=-9 tar caf nmdc-log-service-${version}-linux64.tar.xz nmdc-log-service --owner=0 --group=0
	rm ./nmdc-log-service
	
	echo "Building linux32..."
	GOARCH=386 GOOS=linux go build -a -ldflags -s -o nmdc-log-service
	sanitise nmdc-log-service
	XZ_OPT=-9 tar caf nmdc-log-service-${version}-linux32.tar.xz nmdc-log-service --owner=0 --group=0
	rm ./nmdc-log-service
	
	echo "Build complete."
}

main "$@"
