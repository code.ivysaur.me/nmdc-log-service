# nmdc-log-service

![](https://img.shields.io/badge/written%20in-golang-blue)

A logging service for NMDC hubs.

It logs public chat messages to a file, categorised by months. Binaries are provided for Windows/Linux amd64/i386.

## Usage

```
$nmdc-log-service -Help

Usage of nmdc-log-service:
  -Debug
        Print additional information on stdout
  -Dir string
        Output directory (default ".")
  -LogConnectionState
        Include connection state changes in log (default true)
  -Nick string
        Nick (default "nmdc-log-service")
  -PMResponse string
        Message to respond with on PM (default "This is an automated service. For enquiries, please contact an administrator.")
  -Password string
        Registered nick password
  -Server string
        Addresses to connect to (comma-separated)
  -VerifyTLS
        Verify TLS certificates (default true)
```

## Changelog

master
- Convert to Go Modules, upgrade `libnmdc` to `v0.18.0`

2016-04-16 1.0.4
- Enhancement: Upgrade `libnmdc` from `r5` to `r6`
- Include a sample systemd unit script in source archive
- Fix an issue showing zero connected hubs in user tag
- Fix a cosmetic issue with logging repeated identical connection failure messages
- [⬇️ nmdc-log-service-1.0.4-win64.7z](dist-archive/nmdc-log-service-1.0.4-win64.7z) *(873.36 KiB)*
- [⬇️ nmdc-log-service-1.0.4-win32.7z](dist-archive/nmdc-log-service-1.0.4-win32.7z) *(804.14 KiB)*
- [⬇️ nmdc-log-service-1.0.4-src.7z](dist-archive/nmdc-log-service-1.0.4-src.7z) *(2.05 KiB)*
- [⬇️ nmdc-log-service-1.0.4-linux64.tar.xz](dist-archive/nmdc-log-service-1.0.4-linux64.tar.xz) *(965.75 KiB)*
- [⬇️ nmdc-log-service-1.0.4-linux32.tar.xz](dist-archive/nmdc-log-service-1.0.4-linux32.tar.xz) *(903.36 KiB)*

2016-04-04 1.0.3
- Enhancement: Upgrade `libnmdc` from `r4` to `r5`
- [⬇️ nmdc-log-service-1.0.3-win64.7z](dist-archive/nmdc-log-service-1.0.3-win64.7z) *(873.26 KiB)*
- [⬇️ nmdc-log-service-1.0.3-win32.7z](dist-archive/nmdc-log-service-1.0.3-win32.7z) *(803.45 KiB)*
- [⬇️ nmdc-log-service-1.0.3-linux64.tar.xz](dist-archive/nmdc-log-service-1.0.3-linux64.tar.xz) *(965.26 KiB)*
- [⬇️ nmdc-log-service-1.0.3-linux32.tar.xz](dist-archive/nmdc-log-service-1.0.3-linux32.tar.xz) *(902.95 KiB)*

2016-04-03 1.0.2
- Enhancement: Upgrade `libnmdc` from `r3` to `r4`
- [⬇️ nmdc-log-service-1.0.2-win64.7z](dist-archive/nmdc-log-service-1.0.2-win64.7z) *(872.64 KiB)*
- [⬇️ nmdc-log-service-1.0.2-win32.7z](dist-archive/nmdc-log-service-1.0.2-win32.7z) *(802.16 KiB)*
- [⬇️ nmdc-log-service-1.0.2-linux64.tar.xz](dist-archive/nmdc-log-service-1.0.2-linux64.tar.xz) *(965.06 KiB)*
- [⬇️ nmdc-log-service-1.0.2-linux32.tar.xz](dist-archive/nmdc-log-service-1.0.2-linux32.tar.xz) *(902.40 KiB)*

2016-04-03 1.0.1
- Enhancement: Add `-VerifyTLS` option
- Enhancement: Upgrade `libnmdc` from `r2` to `r3`
- Fix an issue writing log files on Linux
- Fix a cosmetic issue with error message formatting
- [⬇️ nmdc-log-service-1.0.1-win64.7z](dist-archive/nmdc-log-service-1.0.1-win64.7z) *(874.59 KiB)*
- [⬇️ nmdc-log-service-1.0.1-win32.7z](dist-archive/nmdc-log-service-1.0.1-win32.7z) *(802.05 KiB)*
- [⬇️ nmdc-log-service-1.0.1-src.7z](dist-archive/nmdc-log-service-1.0.1-src.7z) *(2.13 KiB)*
- [⬇️ nmdc-log-service-1.0.1-linux64.tar.xz](dist-archive/nmdc-log-service-1.0.1-linux64.tar.xz) *(965.03 KiB)*
- [⬇️ nmdc-log-service-1.0.1-linux32.tar.xz](dist-archive/nmdc-log-service-1.0.1-linux32.tar.xz) *(902.57 KiB)*

2016-04-02 1.0.0
- Initial public release
- [⬇️ nmdc-log-service-1.0.0-win64.7z](dist-archive/nmdc-log-service-1.0.0-win64.7z) *(872.66 KiB)*
- [⬇️ nmdc-log-service-1.0.0-win32.7z](dist-archive/nmdc-log-service-1.0.0-win32.7z) *(802.90 KiB)*
- [⬇️ nmdc-log-service-1.0.0-src.7z](dist-archive/nmdc-log-service-1.0.0-src.7z) *(2.05 KiB)*
- [⬇️ nmdc-log-service-1.0.0-linux64.tar.xz](dist-archive/nmdc-log-service-1.0.0-linux64.tar.xz) *(964.44 KiB)*
- [⬇️ nmdc-log-service-1.0.0-linux32.tar.xz](dist-archive/nmdc-log-service-1.0.0-linux32.tar.xz) *(902.47 KiB)*
